package com.example.employeeapp.util;

public interface NetworkCallBack<T> {
      void onDataObserved(NetworkResponse<T> response);
}
