package com.example.employeeapp.util;

public class NetworkResponse<T>{
    private NetworkStatus status;
    private T data;
    private String message;

    public NetworkResponse(NetworkStatus status, T data, String message){
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <P> NetworkResponse<P> onSuccess(P data){
        return new NetworkResponse(NetworkStatus.SUCCESS, data, null);
    }
    public static NetworkResponse onError(String message){
        return new NetworkResponse(NetworkStatus.SUCCESS, null, message);
    }
    public static NetworkResponse onLoading(){
        return new NetworkResponse(NetworkStatus.LOADING, null, null);
    }



    public NetworkStatus getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }


    public enum NetworkStatus{
        LOADING,SUCCESS, ERROR
    }

}
