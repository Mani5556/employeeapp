package com.example.employeeapp.util;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.employeeapp.repository.EmployeeRepository;
import com.example.employeeapp.retrofit.EmployeeRetrofitClient;
import com.example.employeeapp.ui.home.HomeViewModel;

import java.lang.reflect.InvocationTargetException;

public class EmployeeViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (HomeViewModel.class.isAssignableFrom(modelClass)) {
            try {
                return modelClass.getConstructor(EmployeeRepository.class).newInstance(new EmployeeRepository(
                        EmployeeRetrofitClient.getInstance()
                ));
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("No class found " + modelClass, e);
            }
        } else{
            throw new RuntimeException("No class found " + modelClass);
        }
    }
}
