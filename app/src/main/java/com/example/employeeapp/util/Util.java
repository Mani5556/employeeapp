package com.example.employeeapp.util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import com.example.employeeapp.model.Employee;

public class Util {
    public  static Intent getEmailIntent(String email){
        Intent intent = new Intent(Intent.ACTION_SEND);
        String[] recipients = {email};
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Test Subject");
        intent.putExtra(Intent.EXTRA_TEXT, "Body of the content...");
        intent.setType("text/html");
        intent.setPackage("com.google.android.gm");
        return intent;
    }

    public  static Intent getPhoneIntent(String phoneNumber){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" +phoneNumber));
        return intent;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }
}
