package com.example.employeeapp.ui.home;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.employeeapp.model.Employee;
import com.example.employeeapp.repository.EmployeeRepository;
import com.example.employeeapp.util.NetworkResponse;

import java.util.List;

public class HomeViewModel extends ViewModel {
    EmployeeRepository repository;
    public MutableLiveData<NetworkResponse<List<Employee>>> employeesList = new MutableLiveData();
    public MutableLiveData<NetworkResponse<Employee>> employeeObject = new MutableLiveData();

    public HomeViewModel(EmployeeRepository repository) {
        this.repository = repository;
    }

    public void getEmployees() {
        employeesList.setValue(NetworkResponse.onLoading());
        repository.getEmployees(response -> {
            employeesList.setValue(response);
        });
    }

    public void getEmployeeById(int id) {
        employeeObject.setValue(NetworkResponse.onLoading());
        repository.getEmployeeById(id, mResponse -> {
            employeeObject.setValue(mResponse);
        });
    }
}
