package com.example.employeeapp.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeeapp.databinding.ItemEmployeeBinding;
import com.example.employeeapp.model.Employee;

import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.MyViewHolder> {
    private Context context;
    private List<Employee> employees;
    private OnClickListener listener;

    public EmployeeAdapter(Context context, List<Employee> employees,OnClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.employees = employees;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemEmployeeBinding.inflate(LayoutInflater.from(context),parent,false));
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(employees.get(position));
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ItemEmployeeBinding mBinding;

        public MyViewHolder(@NonNull ItemEmployeeBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        void bind(Employee employee) {
            mBinding.setEmployee(employee);

            mBinding.getRoot().setOnClickListener(v->{
                listener.onClick(employee);
            });
            mBinding.tvEmail.setOnClickListener(v->{
                listener.onEmailClick(employee);
            });
        }
    }

    public interface OnClickListener {
        void onClick(Employee employee);

        void onEmailClick(Employee employee);
    }
}
