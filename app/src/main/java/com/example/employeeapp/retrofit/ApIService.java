package com.example.employeeapp.retrofit;

import com.example.employeeapp.model.Employee;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApIService {

    @GET("users")
    public Call<List<Employee>> getEmployees();

    @GET("users/{id}")
    public Call<Employee> getEmployeeById(@Path(value = "id") int id);
}
