package com.example.employeeapp.repository;

import androidx.lifecycle.LiveData;

import com.example.employeeapp.model.Employee;
import com.example.employeeapp.retrofit.ApIService;
import com.example.employeeapp.util.NetworkCallBack;
import com.example.employeeapp.util.NetworkResponse;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeRepository{
    ApIService apIService;
    public EmployeeRepository(ApIService apIService){
       this.apIService = apIService;
    }

    public void getEmployees(NetworkCallBack<List<Employee>> networkCallBack) {
        apIService.getEmployees().enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                if (response.isSuccessful())
                    networkCallBack.onDataObserved(NetworkResponse.onSuccess(response.body()));
                else
                    networkCallBack.onDataObserved(NetworkResponse.onError(response.message()));
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                networkCallBack.onDataObserved(NetworkResponse.onError(t.getMessage()));
            }
        });
    }

    public void getEmployeeById(int id, NetworkCallBack<Employee> networkCallBack) {

        apIService.getEmployeeById(id).enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                if (response.isSuccessful())
                    networkCallBack.onDataObserved(NetworkResponse.onSuccess(response.body()));
                else
                    networkCallBack.onDataObserved(NetworkResponse.onError(response.message()));
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                networkCallBack.onDataObserved(NetworkResponse.onError(t.getMessage()));
            }
        });
    }
}
